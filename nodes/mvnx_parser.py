#!/usr/bin/env python

import xml.etree.ElementTree as ET
import sys


class MvnxParser():
    """
    Utility for parsing mvnx file and accessing body segment data.
    """
    common_prefix1 = '{http://www.xsens.com/mvn/mvnx}'
    common_prefix2 = ''

    def __init__(self, filepath):
        self.data = self.get_all_data(filepath)

    def get_all_data(self, filepath):
        """
        Parse mvnx file, grab branch of XML tree that contains relevant data.
        """
        try:
            mvnx_tree = ET.parse(filepath)
        except Exception:
            raise

        try:
            try:
                self.prefix = self.common_prefix1
                all_data = next(mvnx_tree.iterfind(self.prefix+'subject'))
            except Exception:
                self.prefix = self.common_prefix2
                all_data = next(mvnx_tree.iterfind(self.prefix+'subject'))
        except Exception, e:
            print("Prefix is wrong: inspect mvnx file manually.\n{}".format(e))
            sys.exit(0)
        return all_data

    def get_frame_list(self, filepath):
        frame_list = list(next(self.data.iterfind(self.prefix+'frames')))
        # print('Number of frames: {}'.format(len(frame_list)))
        return frame_list

    def get_segment_list(self, filepath):
        segment_list = list(next(self.data.iterfind(self.prefix+'segments')))
        # print('Number of segments: {}'.format(len(segment_list)))
        # for segment in segment_list:
        #     print(segment.attrib)
        return segment_list

    def get_from_frame(self, frame, tag):
        """
        Use to pull info out of each frame.
        Commonly used: 'position', 'orientation'
        """
        try:
            info = [float(data) for data in next(frame.iterfind(self.prefix+tag)).text.split()]
        except Exception, e:
            print("Error in get_from_frame with tag {}, {}".format(tag, e))
            raise
        return info


if __name__ == '__main__':
    # filepath = "demo-003.mvnx"
    filepath = 'adrien_dataset/stand-up-001.mvnx'
    get_frame_list(filepath)


# old code
# for child in all_data.getchildren():
#     print("Tag: {}. Length: {}".format(child.tag[len(common_prefix):], len(child.getchildren())))

# frame = frame_list[0]
# # TODO put loop starting here
# # for frame in frame_list:
#
# attribs = frame.attrib
# t = attribs['ms']
# orientations = [float(data) for data in next(frame.iterfind(prefix+'orientation')).text.split()]
# positions = [float(data) for data in next(frame.iterfind(prefix+'position')).text.split()]
#
# print(len(orientations))
# print(len(positions))
# # Orientations and positions of segments
#
# seg_id_real = 6
# seg_id = seg_id_real - 1
# seg_name = segment_list[seg_id].attrib['label']
# pos_start_ind = (seg_id - 1)*3
# print('Position of {}: {}'.format(seg_name, positions[pos_start_ind : pos_start_ind+3]))
