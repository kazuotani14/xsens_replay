#!/usr/bin/env python

import xml.etree.ElementTree as ET
from timeit import default_timer as timer
from time import sleep
import signal
import sys
from math import pi

from mvnx_parser import MvnxParser
from transformations import quaternion_from_euler, quaternion_multiply, quaternion_slerp

import rospy
import rospkg
import tf
import tf2_ros
import geometry_msgs.msg
from geometry_msgs.msg import Pose, Point, Vector3
from xsens_replay.msg import XsensWholeBody, XsensSegment, XsensCollision
from std_srvs.srv import SetBool
import os.path
import numpy as np

xsens2icub_seg_transformations = {
    # yaw, pitch, roll (ROS convention)
    # xyz = rgb in rviz
    'RightHand': [pi/2, pi, 0],
    'LeftHand': [pi/2, 0, 0]
}

xsens2hrp2_seg_transformations = {
    'RightHand': [0, 0, 0],
    'LeftHand': [-pi/4, pi/4, 0]
}

xsens2hrp4_seg_transformations = {
    'RightHand': [0, 0, -pi/2],
    'LeftHand': [0, pi/2, pi/2]
#     'LeftHand': [0, 0, pi/2]
}


class XsensTFPublisherNode():
    """
    Takes recorded movements from xsens mocap as mvnx files, publishes TF tree of human body.
    Make sure to export from MVN Studio with velocities, accelerations, CoM included
    Contact events are to be included in a separate txt file
    """
    def __init__(self, filepath):
        mvnx_filepath = filepath + '.mvnx'
        rospy.loginfo("mvnx_tf_publisher: Using file {}".format(mvnx_filepath))

        # Load parameters
        self.slowdown_factor = rospy.get_param("/mvnx_tf_publisher/slowdown_factor", 1.0)  # TODO make this relative path
        self.transform_to_icub_frame = rospy.get_param("/mvnx_tf_publisher/transform_to_icub_frame", False)
        self.transform_to_hrp2_frame = rospy.get_param("/mvnx_tf_publisher/transform_to_hrp2_frame", False)
        self.transform_to_hrp4_frame = rospy.get_param("/mvnx_tf_publisher/transform_to_hrp4_frame", False)
        self.loop_play = rospy.get_param("/mvnx_tf_publisher/loop", False)

        if self.transform_to_icub_frame:
            self.xsens2robot = xsens2icub_seg_transformations
        elif self.transform_to_hrp2_frame:
            self.xsens2robot = xsens2hrp2_seg_transformations
        elif self.transform_to_hrp4_frame:
            self.xsens2robot = xsens2hrp4_seg_transformations
        else:
            self.xsens2robot = []

        self.last_com_pos = np.array([0,0,0])

        rospy.loginfo("mvnx_tf_publisher - speed: {}, tf2r: {}, loop: {}".format(self.slowdown_factor, self.xsens2robot, self.loop_play))

        # Set up ROS stuff
        self.tf_br = tf2_ros.TransformBroadcaster()
        self.tf_st_br = tf2_ros.StaticTransformBroadcaster()
        self.transformer = tf.Transformer(True, rospy.Duration(1.0))
        self.contacts_pub = rospy.Publisher('xsens_contacts', XsensCollision, queue_size=10)
        self.wholebody_pub = rospy.Publisher('xsens_wholebody', XsensWholeBody, queue_size=30)

        self.pause_service = rospy.Service('pause_mvnx', SetBool, self.toggle_playback)
        self.paused = False

        # Read data
        try:
            self.mvnx = MvnxParser(mvnx_filepath)
        except Exception, e:
            rospy.logerr(e)

        # TODO get rid of double read
        self.frame_list = self.mvnx.get_frame_list(mvnx_filepath)
        self.segment_list = self.mvnx.get_segment_list(mvnx_filepath)
        self.firstframe_time = None

        # Check for contact events
        contacts_filepath = filepath + '.txt'
        if os.path.isfile(contacts_filepath):
            rospy.loginfo("loading contacts")
            self.read_contacts_from_file(contacts_filepath)
            print(self.contacts)
        else:
            rospy.loginfo("no contact data")
            self.contacts = {}

        # Publish mvnx stream
        if self.loop_play:
            while not rospy.is_shutdown():
                sleep(2)
                self.publish_frames()
                print('Done publishing mvnx')
        else:
            sleep(4)
            self.publish_frames()
            print("Done publishing mvnx")

    def read_contacts_from_file(self, filepath):
        """
        Assumes txt with each line:
        '<frame>, <is_addition>, <robot_surface>, <env_surface>, <pos_0>,<pos_1>,<pos_2'
        """
        with open(filepath) as f:
            lines = f.readlines()
            info = [line.strip('\n').replace(' ', '').split(',') for line in lines]
            self.contacts = {int(c[0]): [int(c[1]), c[2], c[3], [int(c[4]), int(c[5]), int(c[6])]] for c in info}

    def publish_frames(self):
        """
        Publish CoM position, link TFs, contact events
        """
        rospy.loginfo('Starting mvnx file playback - %.2f secs long.', float(self.frame_list[-1].attrib['time'])/1000.)
        published_ref = False
        self.start = timer()

        for (i, frame) in enumerate(self.frame_list):
            # Skip first few in frame list, since they contain calibration stuff
            if (frame.attrib['type'] != 'normal'):
                continue

            if self.firstframe_time is None:
                self.firstframe_time = float(frame.attrib['time'])/1000.

            while(self.paused):
                sleep(0.1)
                if(rospy.is_shutdown()):
                    break
            if(rospy.is_shutdown()):
                break

            # Only read 1 out of n frames (hacky downsampling)
            if (i%20 != 0):
                continue
            # print(i)
            # print(frame.attrib['index'])

            self.positions = self.mvnx.get_from_frame(frame, tag='position')
            self.orientations = self.mvnx.get_from_frame(frame, tag='orientation')
            # self.velocities = self.mvnx.get_from_frame(frame, tag='velocity')
            # self.accelerations = self.mvnx.get_from_frame(frame, tag='acceleration')
            self.jointangles = self.mvnx.get_from_frame(frame, tag='jointAngle')

            if not published_ref:
                self.publish_reference_frame(frame)
                published_ref = True

            t_frame = float(frame.attrib['time'])/1000.
            adjusted_time = (timer()-self.start) + self.firstframe_time/self.slowdown_factor
            play_time = t_frame/self.slowdown_factor
            while(adjusted_time < play_time):
                sleep(0.003)  # wait until time that frame was recorded
                adjusted_time = (timer()-self.start) + self.firstframe_time/self.slowdown_factor

            self.publish_from_frame(frame, i)

    def publish_reference_frame(self, frame):
        """
        Publishes a global reference frame for xsens segments
        Calculated as midpoint between feet, x-axis pointing forward, z-axis up
        """
        lf_pos = self.get_segment_pos(21)
        ltoe_pos = self.get_segment_pos(22)
        rf_pos = self.get_segment_pos(17)
        rtoe_pos = self.get_segment_pos(18)
        ref_frame_pos = [(lf_pos[0]+rf_pos[0]+ltoe_pos[0]+rtoe_pos[0])/4, 
                         (lf_pos[1]+rf_pos[1]+ltoe_pos[1]+rtoe_pos[1])/4, 0.]

        lf_quat = self.convert_quat_xsens2tf(self.get_segment_rot(21))
        rf_quat = self.convert_quat_xsens2tf(self.get_segment_rot(17))
	ref_quat = quaternion_slerp(lf_quat, rf_quat, 0.5)

        ref_frame_tf = self.make_transform_msg(ref_frame_pos, ref_quat, 'reference_frame')
        self.tf_st_br.sendTransform(ref_frame_tf)

    def publish_from_frame(self, frame, frame_id):
        """
        Publish frames for each body segment as TF and XsensWholeBody msg
        """
        whole_body_msg = XsensWholeBody()
        whole_body_msg.frame = frame_id

        com_msg = Point()
        com_pos = self.mvnx.get_from_frame(frame, tag='centerOfMass')
        com_msg.x, com_msg.y, com_msg.z = com_pos[0], com_pos[1], com_pos[2]
        whole_body_msg.com = com_msg

        # Check and publish contact
        if frame_id in self.contacts:
            rospy.loginfo("Publishing contact")
            col_msg = XsensCollision()
            col_msg.header.stamp = rospy.Time.now()
            col_msg.is_addition = self.contacts[frame_id][0]
            col_msg.robotBodyName = self.contacts[frame_id][1]
            col_msg.envBodyName = self.contacts[frame_id][2]
            pos_vec = Vector3(self.contacts[frame_id][3][0], self.contacts[frame_id][3][1], self.contacts[frame_id][3][2])
            col_msg.position = pos_vec
            self.contacts_pub.publish(col_msg)

        # Link poses
        for seg_id in range(23):   # note that real segment id is this +1
            pos = self.get_segment_pos(seg_id)
            rot = self.convert_quat_xsens2tf(self.get_segment_rot(seg_id))
            # vel = self.get_segment_vel(seg_id)
            # vel[0], vel[1], vel[2] = vel[0]*self.slowdown_factor, vel[1]*self.slowdown_factor, vel[2]*self.slowdown_factor
            # acc = self.get_segment_acc(seg_id)
            name = self.segment_list[seg_id].attrib['label']

            if(len(self.xsens2robot)):
                rot = self.transform_rot_robot_frame(name, rot)

            t = self.make_transform_msg(pos, rot, name)
            self.tf_br.sendTransform(t)

            whole_body_msg.segments[seg_id] = self.make_segment_msg(name, pos, rot, [0,0,0], [0,0,0])

        com_msg = self.check_com(frame)
        whole_body_msg.segments[23] = com_msg

        # Joint angles
        whole_body_msg.joint_angles = self.jointangles

        self.wholebody_pub.publish(whole_body_msg)

    def check_com(self, frame):
        com_pos = self.mvnx.get_from_frame(frame, tag='centerOfMass')

        ref_quat = tf.transformations.quaternion_from_euler(0., 0., 0.)
        com_tf = self.make_transform_msg(com_pos, ref_quat, 'com')
        self.tf_br.sendTransform(com_tf)

        # Low pass filter com position
        alpha = 0.3
        com_pos_arr = np.array(com_pos)
        com_pos_arr = alpha*com_pos_arr + (1-alpha)*self.last_com_pos

        dt = 0.004  # TODO get this from frame?
        # com_vel = ((com_pos_arr - self.last_com_pos) / dt).tolist()
        com_vel = [0, 0, 0]
        self.last_com_pos = com_pos_arr

        com_acc = [0, 0, 0]
        com_msg = self.make_segment_msg('com', com_pos, ref_quat, com_vel, com_acc)
        return com_msg

    def get_segment_pos(self, seg_id):
        pos_start_idx = seg_id*3
        return self.positions[pos_start_idx:pos_start_idx+3]

    def get_segment_rot(self, seg_id):
        """
        Output quaternions will be in wxyz form (xsens convention)
        """
        rot_start_idx = seg_id*4
        return self.orientations[rot_start_idx:rot_start_idx+4]

    def get_segment_vel(self, seg_id):
        pos_start_idx = seg_id*3
        return self.velocities[pos_start_idx:pos_start_idx+3]

    def get_segment_acc(self, seg_id):
        pos_start_idx = seg_id*3
        return self.accelerations[pos_start_idx:pos_start_idx+3]

    def make_segment_msg(self, pos, rot, frame_name):
        seg = xsensmocap_ros.msg.XsensSegment

    def make_transform_msg(self, pos, rot, frame_name):
        """
        Make TransformStamped message from a list of pos and rot values.
        """
        t = geometry_msgs.msg.TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = 'robot_map'
        t.child_frame_id = 'xsens/' + frame_name

        t.transform.translation.x = pos[0]
        t.transform.translation.y = pos[1]
        t.transform.translation.z = pos[2]
        t.transform.rotation.x = rot[0]
        t.transform.rotation.y = rot[1]
        t.transform.rotation.z = rot[2]
        t.transform.rotation.w = rot[3]

        return t

    def make_segment_msg(self, name, pos, rot, vel, acc):
        seg_msg = XsensSegment()
        seg_msg.id = name

        seg_pose = Pose()
        seg_pose.position.x, seg_pose.position.y, seg_pose.position.z = pos[0], pos[1], pos[2]
        seg_pose.orientation.x, seg_pose.orientation.y, seg_pose.orientation.z, seg_pose.orientation.w = rot[0], rot[1], rot[2], rot[3]
        seg_msg.pose = seg_pose

        seg_vel = Vector3()
        seg_acc = Vector3()
        seg_vel.x, seg_vel.y, seg_vel.z = vel[0], vel[1], vel[2]
        seg_acc.x, seg_acc.y, seg_acc.z = acc[0], acc[1], acc[2]
        seg_msg.velocity = seg_vel
        seg_msg.acceleration = seg_acc

        return seg_msg

    def convert_quat_xsens2tf(self, quat):
        """
        Helper function to switch order of quaternion values.
        Xsens uses wxyz, ROS/tf uses xyzw.
        """
        quat_tf = quat
        quat_tf[0], quat_tf[1], quat_tf[2], quat_tf[3] = quat[1], quat[2], quat[3], quat[0]
        return quat_tf

    def transform_rot_robot_frame(self, name, q):
        if name in self.xsens2robot:
            euler = self.xsens2robot[name]
            q_transform = quaternion_from_euler(euler[0], euler[1], euler[2], 'rzyx')
            return quaternion_multiply(q, q_transform)
        else:
            return q

    def toggle_playback(self, req):
        "True:pause, False:play"
        if req.data:
            self.pause_start_time = timer()
            print("Pausing mvnx playback")
            self.paused = True
        else:
            if self.paused:
                pause_end_time = timer()
                self.start += pause_end_time - self.pause_start_time
            print("Resuming mvnx playback")
            self.paused = False
        return [True, ""]


if __name__ == '__main__':
    filename = rospy.get_param("/mvnx_tf_publisher/mvnx_filename")

    rospack = rospkg.RosPack()
    filepath = rospack.get_path('xsens_replay') + '/data/' + filename

    rospy.init_node('mvnx_segtf_publisher')

    try:
        seg_pub_node = XsensTFPublisherNode(filepath)
    except rospy.ROSInterruptException:
        pass
