* Rewrite mvnx parser code using SAX, referencing xsens-mvn
	* <https://stackoverflow.com/questions/42325244/how-to-use-xml-sax-parser-to-read-and-write-a-large-xml>
	* <https://stackoverflow.com/questions/12263029/how-to-get-results-from-xml-sax-parser-in-python>
	* <https://docs.python.org/3/library/xml.sax.reader.html>
	* <https://docs.python.org/2/library/xml.sax.html>
	* <https://stackoverflow.com/questions/6828703/what-is-the-difference-between-sax-and-dom>
	* <https://github.com/robotology-playground/xsens-mvn>
	* <https://github.com/robotology-playground/xsens-mvn/pull/12/files/456903264af79e54545ce3057a641ca06d0f2313#diff-8e93313872bdab825101561d5dbf751c>
	* <https://github.com/robotology-playground/xsens-mvn/pull/12>
	* <https://github.com/robotology/human-dynamics-estimation/issues/48
	* make list of TFs to publish, do them all at once
* move data to Large File Storage
