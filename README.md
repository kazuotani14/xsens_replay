# xsens_replay

ROS node for publishing Xsens motion capture data as TF and ROS messages. Also publishes contact events if provided. See mvnx_tf_publisher.py for more details.

I wrote this because I had some specific use cases (contact detection, joint angle publishing), but most people should use [xsens-mvn](https://github.com/robotology-playground/xsens-mvn) from IIT. 
